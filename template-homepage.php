<?php
/**
 * Template Name: Homepage Template
 */
?>

<?php 
	$hero_video_field = get_post_meta( get_the_ID(), 'hero_video', true );
	$hero_video = wp_get_attachment_url( $hero_video_field );

	$hero_image_field = get_post_meta( get_the_ID(), 'hero_image', true );
	$hero_image = wp_get_attachment_url( $hero_image_field );
?>

<section class="hero" style="background-image: url(<?php echo $hero_image ?>)">
	<?php if ( $hero_video_field ) { ?>
		<video autoplay muted class="fillWidth" poster="<?php echo $hero_image ?>" src="<?php echo $hero_video ?>"></video>
	<?php } ?>

	<div class="hero-content" style="background-image: url(<?php echo $hero_image ?>)">
		<div class="container">
			<img src="<?php echo get_template_directory_uri() ?>/dist/images/logo-hero-1.png">
			<h1>Traversant<br>Group<span class="reg-mark">&reg;</span></h1>
			<p>Expand your influence.<span class="tm-mark">&#8482;</span></p>
		</div>
	</div>

	<div class="site-intro">
		<div class="container">
			<h2>Connecting Leaders. Building Strategies. Delivering Results.</h2>

			<p>We are the proven authority in government and legislative strategy, sales and business consulting. But even more than this, we’re your partner. We know you have lofty government-centric goals and our firsthand experience will help you achieve them.</p>
		</div>
	</div>
</section>

<section class="posts-slider-wrapper">
	<div class="posts-slider-bg"></div>
	<div class="container">
		<div class="posts-slider">
			<?php
				// WP_Query arguments
				$args = array (
					'post_type'              => array( 'post' ),
					'posts_per_page'         => '12',
				);

				// The Query
				$query = new WP_Query( $args );

				// The Loop
				if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
						$query->the_post();
						get_template_part('templates/content');
					}
				} else {
					// no posts found
				}

				// Restore original Post Data
				wp_reset_postdata();
			?>
		</div>
	</div>
	<div class="posts-slider-link">
		<div class="container">
			<a href="<?php echo get_permalink( 2 ); ?>">More Blog Posts</a>
		</div>
	</div>
</section>

<section class="twitter">
	<div class="container">
		<h2>Recent Tweets</h2>
		<div class="twitter-feed">
			<span class="quote-left"></span>
			<?php echo do_shortcode( '[mintweet username="traversantgroup" count="5" type="user" retweets=“0” replies=“0”]' ); ?>
			<span class="quote-right"></span>
		</div>
		<a class="btn btn-default" href="https://twitter.com/traversantgroup" target="_blank">Follow @Traversantgroup</a>
	</div>
</section>

<div class="container">
	<?php while (have_posts()) : the_post(); ?>
	  <?php get_template_part('templates/content', 'page'); ?>
	<?php endwhile; ?>
</div>