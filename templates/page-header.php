<?php use Roots\Sage\Titles; ?>

<?php
	$title_override_field = get_post_meta( get_the_ID(), 'page_title_override', true );
	$title_override = esc_html( $title_override_field );

	$title_paragraph = get_post_meta( get_the_ID(), 'page_title_paragraph', true );
?>

<div class="page-header">
	<div class="container">

		<?php if ( $title_override ) { ?>
			<h1><?php echo $title_override ?></h1>
		<?php } elseif ( is_home() ) { ?>
			<h1>Insight. Experience. Advantage.</h1>
		<?php } else { ?>
			<h1><?php echo Titles\title(); ?></h1>
		<?php } ?>

  </div>

  <?php if ( $title_paragraph ) { ?>
	  <div class="sub-title">
	  	<div class="container">
		  	<p><?php echo $title_paragraph ?></p>
	  	</div>
	  </div>
	  <?php } ?>
</div>
