<footer class="content-info">
  <div class="container">
  	<div class="social">
  	  <ul class="list-inline list-unstyled">
  	    <li><a href="https://twitter.com/traversantgroup" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
  	    <li><a href="https://www.linkedin.com/in/jenniferswoods" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
  	  </ul>
  	</div>
    <span class="copyrights">&copy; <?php echo date("Y") ?> · Traversant Group. </span> <span><span class="rights">All Rights Reserved.</span><span class="footer-divider first">|</span><a href="<?php echo get_permalink( 8 );?>">Contact</a><span class="footer-divider">|</span><a href="<?php echo get_permalink( 649 ); ?>">Sitemap</a>

    <div class="legal"><span>Traversant Group and the “t” logo are registered trademarks, and “Expand Your Influence” is a trademark of Arizona Government Strategies, LLC.</span></div>
  </div>
</footer>