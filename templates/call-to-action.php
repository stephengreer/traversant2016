<?php
	$display_call_to_action = get_post_meta( get_the_ID(), 'display_call_to_action', true );

	$cta_title_field = get_post_meta( get_the_ID(), 'cta_title', true );
	$cta_title = esc_html( $cta_title_field );

	$cta_content_field = get_post_meta( get_the_ID(), 'cta_content', true );
	$cta_content = esc_html( $cta_content_field );

	$cta_button_label_field = get_post_meta( get_the_ID(), 'cta_button_label', true );
	$cta_button_label = esc_html( $cta_button_label_field );

	$cta_button_link = get_post_meta( get_the_ID(), 'cta_button_link', true );
?>

<?php if ( $display_call_to_action ) { ?>
	<div class="page-cta">
	  <div class="content-container">
	    <h4><?php echo $cta_title ?></h4>
	    <p><?php echo $cta_content ?></p>
	    <a href="<?php echo $cta_button_link ?>" class="btn btn-primary"><?php echo $cta_button_label ?></a>
	  </div>
	</div>
<?php }