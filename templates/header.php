<header class="banner navbar navbar-default navbar-fixed-top" role="banner">

  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?php echo __('Toggle navigation', 'sage'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri()?>/dist/images/logo.png"></a>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav']);
      endif;
      ?>

      <div class="navbar-nav mobile-subscribe">
        <button class="subscribe-btn">Subscribe To The Blog</button>

        <div class="subscribe-form">
          <?php if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 5 ); } ?>
        </div>
      </div>
    </nav>
  </div>

  <div class="float-right">
    <div class="social">
      <ul class="list-inline list-unstyled">
        <li><a href="https://twitter.com/traversantgroup" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="https://www.linkedin.com/company/traversant-group" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
      </ul>
    </div>

    <div class="subscribe">
      <button class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>

      <div class="subscribe-search">
        <button class="subscribe-btn">Subscribe To The Blog</button>

        <div class="subscribe-form">
          <?php if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 5 ); } ?>
        </div>

        <?php echo get_search_form() ?>
      </div>
    </div>
  </div>

</header>
<div class="header-spacing"></div>