<?php
	$post_excerpt = get_post_meta( get_the_ID(), 'post_excerpt', true );
  $post_excerpt_trimmed = wp_trim_words( $post_excerpt, 35, '...' );
  $post_slug = get_post_field( 'post_name', get_post() );
  $post_title = get_post_field( 'post_title', get_post() );
?>

<article <?php post_class(); ?>>
	<div class="featured-image">
		<?php the_post_thumbnail( 'blog-thumbnail' ); ?>
	</div>
  <header>
	  <?php get_template_part( 'templates/entry-meta' ); ?>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
  </header>
  <div class="entry-summary">
  	<?php if ( $post_excerpt ) {
  		echo wpautop( $post_excerpt_trimmed );
  	} else {
	    echo wpautop( the_excerpt() ); 
    } ?>
  </div>
  <div class="entry-more">
  	<a class="btn btn-primary btn-sm" href="<?php the_permalink(); ?>">Read More</a>
  	<button class="btn btn-sm share-button"><i class="fa fa-share-square-o" aria-hidden="true"></i></button>
  	<div class="share-menu">
  		<a target="_blank" href="http://twitter.com/share?text=<?php echo $post_title ?>&url=http://traversantgroup.com/<?php echo $post_slug ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
  		<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=http://traversantgroup.com/<?php echo $post_slug ?>&title=<?php echo $post_title ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
      <a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=traversantgroup.com/<?php echo $post_slug ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
  	</div>
  </div>
</article>