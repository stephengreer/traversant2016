<?php 
  $post_excerpt = get_post_meta( get_the_ID(), 'post_excerpt', true );
  $featured_image = get_the_post_thumbnail( get_the_ID(), 'blog-single' );
?>


<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>

    <header class="page-header">
      <div class="container">
        <?php echo $featured_image ?>
        <div class="page-header-content">
          <h1 class="entry-title"><?php the_title(); ?></h1>
          <?php get_template_part('templates/entry-meta'); ?>
        </div>
      </div>
    </header>

    <div class="entry-content content-container">
      <div class="post-intro">
        <?php if ( $post_excerpt ) {
          echo wpautop( $post_excerpt );
        } ?>
      </div>

      <?php the_content(); ?>

      <p><a class="btn btn-primary back-button" href="<?php echo get_permalink( 2 ); ?>">Back To Blog Posts</a></p>
    </div>

    <footer class="content-container">
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>

  </article>
<?php endwhile; ?>
