<?php
/**
 * Template Name: Expertise Template
 */
?>


<?php get_template_part('templates/page', 'header'); ?>

<?php if( have_rows('expertise_sections') ): ?>

	<div class="container">

	<?php while( have_rows('expertise_sections') ): the_row(); 

		$expertise_image = get_sub_field('expertise_image');
		$expertise_title = get_sub_field('expertise_title');
		$expertise_content = get_sub_field('expertise_content');

		?>

		<section class="expertise">

			<?php if( $expertise_image ): ?>
				<div class="expertise-image" style="background-image: url(<?php echo $expertise_image['url']; ?>);"></div>
			<?php endif; ?>

			<div class="expertise-content">
				<h2><?php echo esc_html( $expertise_title ); ?></h2>
				<?php echo $expertise_content; ?>
			</div>

		</section>

	<?php endwhile; ?>

	</div>

<?php endif; ?>
