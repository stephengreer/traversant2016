<?php
/**
 * Template Name: Team Template
 */
?>

<?php get_template_part('templates/page', 'header'); ?>

<?php 
	$team_members = get_field('team_members');

	$team_members_row_1 = array_slice($team_members, 0, 2);
	$team_members_row_2 = array_slice($team_members, 2, 4);
	$team_members_row_3 = array_slice($team_members, 4, 6);
	$team_members_row_4 = array_slice($team_members, 6, 8);
	$team_members_row_5 = array_slice($team_members, 8, 10);
	$team_members_row_6 = array_slice($team_members, 10, 12);
	$team_members_row_7 = array_slice($team_members, 12, 14);
	$team_members_row_8 = array_slice($team_members, 14, 16);
	$team_members_row_9 = array_slice($team_members, 16, 18);
	$team_members_row_10 = array_slice($team_members, 18, 20);

?>

<?php if ($team_members_row_1) { ?>
	<div id="team-row-1" class="team-row">
		
		<div class="container">

			<?php	$i = 0; foreach ($team_members_row_1 as $row) {
				$i++;
			?>

				<div id="team-member-<?php echo $i; ?>" class="team-member">
					<div class="team-member-photo">
						<img src="<?php echo $row['photo']['url'] ?>" alt="<?php echo $row['photo']['alt'] ?>" />
					</div>

					<div class="team-member-info">
						<h2 class="name"><?php echo $row['name']; ?></h2>
						<h3 class="position"><?php echo $row['position']; ?></h3>
						<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
					</div>

					<div class="bio-mobile">
						<div class="container">
							<?php echo $row['bio'] ?>
						</div>
					</div>
				</div>

			<?php } ?>
		</div>


		<?php	$i = 0; foreach ($team_members_row_1 as $row) {
			$i++;
		?>

		  <div id="team-bio-<?php echo $i; ?>" class="team-member-bio">
				<div class="container">
					<?php echo $row['bio'] ?>
				</div>
			</div>

		<?php } ?>
	</div>
<?php } ?>


<?php if ($team_members_row_2) { ?>
	<div id="team-row-2" class="team-row">
		
		<div class="container">

			<?php	$i = 2; foreach ($team_members_row_2 as $row) {
				$i++;
			?>

				<div id="team-member-<?php echo $i; ?>" class="team-member">
					<div class="team-member-photo">
						<img src="<?php echo $row['photo']['url'] ?>" alt="<?php echo $row['photo']['alt'] ?>" />
					</div>

					<div class="team-member-info">
						<h2 class="name"><?php echo $row['name']; ?></h2>
						<h3 class="position"><?php echo $row['position']; ?></h3>
						<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
					</div>

					<div class="bio-mobile">
						<div class="container">
							<?php echo $row['bio'] ?>
						</div>
					</div>
				</div>

			<?php } ?>
		</div>


		<?php	$i = 2; foreach ($team_members_row_2 as $row) {
			$i++;
		?>

		  <div id="team-bio-<?php echo $i; ?>" class="team-member-bio">
				<div class="container">
					<?php echo $row['bio'] ?>
				</div>
			</div>

		<?php } ?>
	</div>
<?php } ?>


<?php if ($team_members_row_3) { ?>
	<div id="team-row-3" class="team-row">
		
		<div class="container">

			<?php	$i = 4; foreach ($team_members_row_3 as $row) {
				$i++;
			?>

				<div id="team-member-<?php echo $i; ?>" class="team-member">
					<div class="team-member-photo">
						<img src="<?php echo $row['photo']['url'] ?>" alt="<?php echo $row['photo']['alt'] ?>" />
					</div>

					<div class="team-member-info">
						<h2 class="name"><?php echo $row['name']; ?></h2>
						<h3 class="position"><?php echo $row['position']; ?></h3>
						<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
					</div>

					<div class="bio-mobile">
						<div class="container">
							<?php echo $row['bio'] ?>
						</div>
					</div>
				</div>

			<?php } ?>
		</div>


		<?php	$i = 4; foreach ($team_members_row_3 as $row) {
			$i++;
		?>

		  <div id="team-bio-<?php echo $i; ?>" class="team-member-bio">
				<div class="container">
					<?php echo $row['bio'] ?>
				</div>
			</div>

		<?php } ?>
	</div>
<?php } ?>


<?php if ($team_members_row_4) { ?>
	<div id="team-row-4" class="team-row">
		
		<div class="container">

			<?php	$i = 6; foreach ($team_members_row_4 as $row) {
				$i++;
			?>

				<div id="team-member-<?php echo $i; ?>" class="team-member">
					<div class="team-member-photo">
						<img src="<?php echo $row['photo']['url'] ?>" alt="<?php echo $row['photo']['alt'] ?>" />
					</div>

					<div class="team-member-info">
						<h2 class="name"><?php echo $row['name']; ?></h2>
						<h3 class="position"><?php echo $row['position']; ?></h3>
						<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
					</div>

					<div class="bio-mobile">
						<div class="container">
							<?php echo $row['bio'] ?>
						</div>
					</div>
				</div>

			<?php } ?>
		</div>


		<?php	$i = 6; foreach ($team_members_row_4 as $row) {
			$i++;
		?>

		  <div id="team-bio-<?php echo $i; ?>" class="team-member-bio">
				<div class="container">
					<?php echo $row['bio'] ?>
				</div>
			</div>

		<?php } ?>
	</div>
<?php } ?>


<?php if ($team_members_row_5) { ?>
	<div id="team-row-5" class="team-row">
		
		<div class="container">

			<?php	$i = 8; foreach ($team_members_row_5 as $row) {
				$i++;
			?>

				<div id="team-member-<?php echo $i; ?>" class="team-member">
					<div class="team-member-photo">
						<img src="<?php echo $row['photo']['url'] ?>" alt="<?php echo $row['photo']['alt'] ?>" />
					</div>

					<div class="team-member-info">
						<h2 class="name"><?php echo $row['name']; ?></h2>
						<h3 class="position"><?php echo $row['position']; ?></h3>
						<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
					</div>

					<div class="bio-mobile">
						<div class="container">
							<?php echo $row['bio'] ?>
						</div>
					</div>
				</div>

			<?php } ?>
		</div>


		<?php	$i = 8; foreach ($team_members_row_5 as $row) {
			$i++;
		?>

		  <div id="team-bio-<?php echo $i; ?>" class="team-member-bio">
				<div class="container">
					<?php echo $row['bio'] ?>
				</div>
			</div>

		<?php } ?>
	</div>
<?php } ?>


<?php if ($team_members_row_6) { ?>
	<div id="team-row-6" class="team-row">
		
		<div class="container">

			<?php	$i = 10; foreach ($team_members_row_6 as $row) {
				$i++;
			?>

				<div id="team-member-<?php echo $i; ?>" class="team-member">
					<div class="team-member-photo">
						<img src="<?php echo $row['photo']['url'] ?>" alt="<?php echo $row['photo']['alt'] ?>" />
					</div>

					<div class="team-member-info">
						<h2 class="name"><?php echo $row['name']; ?></h2>
						<h3 class="position"><?php echo $row['position']; ?></h3>
						<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
					</div>

					<div class="bio-mobile">
						<div class="container">
							<?php echo $row['bio'] ?>
						</div>
					</div>
				</div>

			<?php } ?>
		</div>


		<?php	$i = 10; foreach ($team_members_row_6 as $row) {
			$i++;
		?>

		  <div id="team-bio-<?php echo $i; ?>" class="team-member-bio">
				<div class="container">
					<?php echo $row['bio'] ?>
				</div>
			</div>

		<?php } ?>
	</div>
<?php } ?>


<?php if ($team_members_row_7) { ?>
	<div id="team-row-7" class="team-row">
		
		<div class="container">

			<?php	$i = 12; foreach ($team_members_row_7 as $row) {
				$i++;
			?>

				<div id="team-member-<?php echo $i; ?>" class="team-member">
					<div class="team-member-photo">
						<img src="<?php echo $row['photo']['url'] ?>" alt="<?php echo $row['photo']['alt'] ?>" />
					</div>

					<div class="team-member-info">
						<h2 class="name"><?php echo $row['name']; ?></h2>
						<h3 class="position"><?php echo $row['position']; ?></h3>
						<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
					</div>

					<div class="bio-mobile">
						<div class="container">
							<?php echo $row['bio'] ?>
						</div>
					</div>
				</div>

			<?php } ?>
		</div>


		<?php	$i = 12; foreach ($team_members_row_7 as $row) {
			$i++;
		?>

		  <div id="team-bio-<?php echo $i; ?>" class="team-member-bio">
				<div class="container">
					<?php echo $row['bio'] ?>
				</div>
			</div>

		<?php } ?>
	</div>
<?php } ?>


<?php if ($team_members_row_8) { ?>
	<div id="team-row-8" class="team-row">
		
		<div class="container">

			<?php	$i = 14; foreach ($team_members_row_8 as $row) {
				$i++;
			?>

				<div id="team-member-<?php echo $i; ?>" class="team-member">
					<div class="team-member-photo">
						<img src="<?php echo $row['photo']['url'] ?>" alt="<?php echo $row['photo']['alt'] ?>" />
					</div>

					<div class="team-member-info">
						<h2 class="name"><?php echo $row['name']; ?></h2>
						<h3 class="position"><?php echo $row['position']; ?></h3>
						<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
					</div>

					<div class="bio-mobile">
						<div class="container">
							<?php echo $row['bio'] ?>
						</div>
					</div>
				</div>

			<?php } ?>
		</div>


		<?php	$i = 14; foreach ($team_members_row_8 as $row) {
			$i++;
		?>

		  <div id="team-bio-<?php echo $i; ?>" class="team-member-bio">
				<div class="container">
					<?php echo $row['bio'] ?>
				</div>
			</div>

		<?php } ?>
	</div>
<?php } ?>


<?php if ($team_members_row_9) { ?>
	<div id="team-row-9" class="team-row">
		
		<div class="container">

			<?php	$i = 16; foreach ($team_members_row_9 as $row) {
				$i++;
			?>

				<div id="team-member-<?php echo $i; ?>" class="team-member">
					<div class="team-member-photo">
						<img src="<?php echo $row['photo']['url'] ?>" alt="<?php echo $row['photo']['alt'] ?>" />
					</div>

					<div class="team-member-info">
						<h2 class="name"><?php echo $row['name']; ?></h2>
						<h3 class="position"><?php echo $row['position']; ?></h3>
						<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
					</div>

					<div class="bio-mobile">
						<div class="container">
							<?php echo $row['bio'] ?>
						</div>
					</div>
				</div>

			<?php } ?>
		</div>


		<?php	$i = 16; foreach ($team_members_row_9 as $row) {
			$i++;
		?>

		  <div id="team-bio-<?php echo $i; ?>" class="team-member-bio">
				<div class="container">
					<?php echo $row['bio'] ?>
				</div>
			</div>

		<?php } ?>
	</div>
<?php } ?>


<?php if ($team_members_row_10) { ?>
	<div id="team-row-10" class="team-row">
		
		<div class="container">

			<?php	$i = 18; foreach ($team_members_row_10 as $row) {
				$i++;
			?>

				<div id="team-member-<?php echo $i; ?>" class="team-member">
					<div class="team-member-photo">
						<img src="<?php echo $row['photo']['url'] ?>" alt="<?php echo $row['photo']['alt'] ?>" />
					</div>

					<div class="team-member-info">
						<h2 class="name"><?php echo $row['name']; ?></h2>
						<h3 class="position"><?php echo $row['position']; ?></h3>
						<i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
					</div>

					<div class="bio-mobile">
						<div class="container">
							<?php echo $row['bio'] ?>
						</div>
					</div>
				</div>

			<?php } ?>
		</div>


		<?php	$i = 18; foreach ($team_members_row_10 as $row) {
			$i++;
		?>

		  <div id="team-bio-<?php echo $i; ?>" class="team-member-bio">
				<div class="container">
					<?php echo $row['bio'] ?>
				</div>
			</div>

		<?php } ?>
	</div>
<?php } ?>