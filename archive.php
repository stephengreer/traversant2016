<?php get_template_part('templates/page', 'header'); ?>

<div class="posts-filter">
	<div class="container">
		<button class="toggle-filters">Filter by <i class="fa fa-sort" aria-hidden="true"></i></button>
		<?php echo do_shortcode( '[searchandfilter id="675"]' ); ?>
	</div>
</div>

<div class="container">
	<?php if (!have_posts()) : ?>
	  <div class="alert alert-warning">
	    <?php _e('Sorry, no results were found.', 'sage'); ?>
	  </div>
	<?php endif; ?>

	<?php while (have_posts()) : the_post(); ?>
	  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
	<?php endwhile; ?>

	<?php
	if (function_exists("wp_bs_pagination"))
	  {
	    wp_bs_pagination();
	  }
	?>
</div>