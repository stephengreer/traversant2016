<?php
/**
 * Template Name: Contact Template
 */
?>


<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>

  <div class="container">
	  <?php get_template_part('templates/content', 'page'); ?>

	  <div class="contact-page">

		  <div class="contact-content row">

		  	<div class="col-sm-6">
		  		<h2>Address & Phone</h2>
		  		<p><a href="https://goo.gl/maps/3PPhKiiwHPK2" target="_blank">2415 E. Camelback Road<br>
		  		Suite 700<br>
		  		Phoenix, AZ 85016</a></p>

		  		<p><a href="tel:+16023437465">602-343-7465</a></p>

		  		<p><a href="https://goo.gl/maps/4AagFoZ4Nk22" target="_blank"><img src="<?php echo get_template_directory_uri();?>/dist/images/traversant-contact-map-location.jpg"></a></p>
		  	</div>

		  	<div class="col-sm-6 contact-form">
		  		<span class="required">* Required Fields</span>
		  		<h2>Send Us A Message</h2>
		  		<?php if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 1 ); } ?>
		  	</div>

		  </div>

	  </div>
  </div>
<?php endwhile; ?>