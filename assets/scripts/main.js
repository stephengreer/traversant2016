/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        document.documentElement.setAttribute("data-browser", navigator.userAgent);

        $('.search-btn').click(function(){
          $("#searchform").toggleClass("active");
          $(".subscribe-form").removeClass("active");
        });

        $('.subscribe-btn').click(function(){
          $(".subscribe-form").toggleClass("active");
        });

        $( window ).scroll(function(){
          $(".subscribe-form").removeClass("active");
        });

        function hideSubscribeForm() {
          var WindowWidth = $( window ).width();

          if ( WindowWidth > 767 ) {
            $( '.mobile-subscribe .subscribe-form' ).remove();
          } else {
            $( '.float-right .subscribe-form' ).remove();
          }
        }

        $(document).ready(hideSubscribeForm);
        $(window).resize(hideSubscribeForm);


        $('.team-member').click(function(){
          var windowWidth = $( window ).width();
          var itemNum = $(this).attr('id').replace ( /[^\d.]/g, '' );
          var scrollLenght = $(this).offset().top;

          $('.team-member').not(this).removeClass("active");
          $(this).toggleClass("active");

          $('.team-member-bio').not('#team-bio-' + itemNum).removeClass("active");
          $('#team-bio-' + itemNum).toggleClass("active");

          if (windowWidth < 769) {
            $('body').scrollTop( scrollLenght - 100 );
          }

        });


        $('.share-button').click(function(){
          $(this).parent().addClass("active");
        });

        $('.share-menu a').click(function(){
          $('.entry-more').removeClass("active");
        });


        function blogImageMargin() {
          var WindowWidth = $( window ).width();
          var ContainerWidth = $(".container").outerWidth();

          var MarginWidth = (WindowWidth - ContainerWidth + 30) / 2;

          $('body.single .page-header img').css("margin-left", - MarginWidth);
        }

        $(document).ready(blogImageMargin);
        $(window).on('resize',blogImageMargin);


        function expertiseImageMargin() {
          var WindowWidth = $( window ).width();
          var ContainerWidth = $(".container").outerWidth();

          var MarginWidth = (WindowWidth - ContainerWidth + 30) / 2;

          $('.page-template-template-expertise .expertise-image').css("margin-left", - MarginWidth);
        }

        $(document).ready(expertiseImageMargin);
        $(window).on('resize',expertiseImageMargin);


        $('.sf-input-select').selectpicker({
          dropupAuto: false,
        });

        $('.toggle-filters').click(function(){
          $("#search-filter-form-675").toggleClass("active");
        });

        // $('.subscribe-form input[type=submit]').click(function(){
        //   $('.subscribe-form').removeClass("active");
        // });


        /**
         * detect IE
         * returns version of IE or false, if browser is not Internet Explorer
         */
        function detectIE() {
          var ua = window.navigator.userAgent;

          // Test values; Uncomment to check result …

          // IE 10
          // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
          
          // IE 11
          // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
          
          // Edge 12 (Spartan)
          // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
          
          // Edge 13
          // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

          var msie = ua.indexOf('MSIE ');
          if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
          }

          var trident = ua.indexOf('Trident/');
          if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
          }

          var edge = ua.indexOf('Edge/');
          if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
          }

          // other browser
          return false;
        }

        if ( detectIE() ) {
          $('.hero video').remove();
        }


        // Sticky Footer

        function stickyFooter() {
          var windowHeight = $(window).height();
          var contentHeight = $(".wrap").height();
          var navHeight = $(".navbar").height();
          var footerHeight = $(".content-info").height();

          var marginTop = windowHeight - ( contentHeight + navHeight + footerHeight + 80);

          if ( marginTop > 30 ) {
            $('.page-template-template-sitemap .content-info').css("margin-top", marginTop);
            $('.blog .content-info').css("margin-top", marginTop);
          }
        }

        if ( $( ".page-template-template-sitemap" ) || $( ".blog" ).length ) {
          $(document).ready(stickyFooter);
          $(window).on('resize',stickyFooter);
        }

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {

        var isMobile = {
          Android: function() {
              return navigator.userAgent.match(/Android/i);
          },
          BlackBerry: function() {
              return navigator.userAgent.match(/BlackBerry/i);
          },
          iOS: function() {
              return navigator.userAgent.match(/iPhone|iPad|iPod/i);
          },
          Opera: function() {
              return navigator.userAgent.match(/Opera Mini/i);
          },
          Windows: function() {
              return navigator.userAgent.match(/IEMobile/i);
          },
          any: function() {
              return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
          }
        };

        $(function(){
          if( isMobile.any() ){
            $('.hero video').remove();
          }
        });


        function sliderImageHeight() {
          var ImageHeight = $(".posts-slider img").outerHeight();

          var arrowTop = ImageHeight / 2;

          $('.posts-slider-bg').css("height", ImageHeight);
          $('.slick-arrow').css("top", arrowTop);
        }

        $(document).ready(sliderImageHeight);
        $(".posts-slider img").on('load', sliderImageHeight);
        $(window).on('resize', sliderImageHeight);


        $('#tweets').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          cssEase: 'ease-in-out',
          autoplay: true,
          autoplaySpeed: 8000,
          arrows: false,
          fade: true,
        });

        $(document).ready(function() {
          $('.hero-content .container').addClass("active");
        });


        $('.posts-slider').slick({
          slidesToShow: 3,
          slidesToScroll: 3,
          cssEase: 'ease-in-out',
          autoplay: false,
          arrows: true,
          infinite: false,
          prevArrow: '<button type="button" class="slick-prev">Prev<span class="hover"></span></button>',
          nextArrow: '<button type="button" class="slick-next">Next<span class="hover"></span></button>',
          responsive: [
            {
              breakpoint: 1280,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
              }
            },
            {
              breakpoint: 769,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 533,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                adaptiveHeight: true,
              }
            }
          ]
        });

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
