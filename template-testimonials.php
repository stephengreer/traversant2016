<?php
/**
 * Template Name: Testimonals Template
 */
?>

<?php get_template_part('templates/page', 'header'); ?>

<?php if( have_rows('testimonials') ): ?>

	<div class="content-container">

	<?php while( have_rows('testimonials') ): the_row(); 

		$testimonial_content = get_sub_field('testimonial');
		$testimonial_name = get_sub_field('name');
		$testimonial_title = get_sub_field('title');

		?>

		<article class="testimonial-item">

			<?php echo $testimonial_content; ?>

			<p class="title"><?php echo esc_html( $testimonial_title ); ?></p>
			<p class="name"><?php echo esc_html( $testimonial_name ); ?></p>

		</article>

	<?php endwhile; ?>

	</div>

<?php endif; ?>
