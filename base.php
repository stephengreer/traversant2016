<?php ob_start(); ?>

<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="wrap" role="document">
      <div class="content">
        <main>
          <?php include Wrapper\template_path(); ?>
          <?php get_template_part( 'templates/call-to-action' ); ?>
        </main><!-- /.main -->
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
    
    <!-- Start of Async HubSpot Analytics Code -->
      <script type=3D"text/javascript">
        (function(d,s,i,r) {
          if (d.getElementById(i)){return;}
          var n=3Dd.createElement(s),e=3Dd.getElementsByTagName(s)[0];
          n.id=3Di;n.src=3D'//js.hs-analytics.net/analytics/'+(Math.ceil(new
    Date()/r)*r)+'/2320377.js';
          e.parentNode.insertBefore(n, e);
        })(document,"script","hs-analytics",300000);
      </script>
    <!-- End of Async HubSpot Analytics Code -->
  </body>
</html>
