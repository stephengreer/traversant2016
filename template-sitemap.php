<?php
/**
 * Template Name: Sitemap Template
 */
?>


<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>

  <div class="container">
	  <?php wp_nav_menu(); ?>
  </div>
<?php endwhile; ?>